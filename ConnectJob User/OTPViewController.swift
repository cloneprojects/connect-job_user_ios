//
//  OTPViewController.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 19/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftSpinner
import SwiftyJSON

class OTPViewController: UIViewController {
    @IBOutlet weak var btnverify: UIButton!
    
    @IBOutlet weak var otpField: UITextField!
    var otp : String!
    var email : String!
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()

        otpField.text = otp
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
        }
    }
    
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated:true,completion:nil)
    }
    
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func verify(_ sender: Any) {
        if let text = otpField.text, !text.isEmpty
        {
            let params: Parameters = [
                "email": self.email,
                "otp": otpField.text!
                ]
            SwiftSpinner.show("Sending OTP...")
            let url = "\(Constants.baseURL)/api/forgotpasswordotpcheck"
            Alamofire.request(url,method: .post,parameters:params).responseJSON { response in
                
                if(response.result.isSuccess)
                {
                    SwiftSpinner.hide()
                    if let json = response.result.value {
                        print("OTP VERIFY JSON: \(json)") // serialized json response
                        let jsonResponse = JSON(json)
                        if(jsonResponse["error"].stringValue == "true")
                        {
                            let errorMessage = jsonResponse["error_message"].stringValue
                            self.showAlert(title: "Failed",msg: errorMessage)
                        }
                        else{
//                            let otp = jsonResponse["otp"].stringValue
                            let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetOtpViewController") as! ResetOtpViewController
                            vc.email = self.email
                            self.present(vc, animated: true, completion: nil)
                        }
                    }
                }
                else{
                    SwiftSpinner.hide()
                    print(response.error.debugDescription)
                    self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                }
                
            }
        }
        else{
            showAlert(title: "Validation Failed",msg: "Invalid Email")
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
