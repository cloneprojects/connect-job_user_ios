//
//  Constants.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 21/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit
struct Constants {
    //App Constants
    static var baseURL = "http://35.180.94.105/uber_test/public"
    static var adminBaseURL = "http://35.180.94.105/uber_test/public/admin"
    static var mapsKey = "AIzaSyAavm-0yC6bQUAw_Zt84rTcWBA-nWUiWmg"
    static var socketURL = "http://35.180.94.105:3000/"
    static var placesKey = "AIzaSyAavm-0yC6bQUAw_Zt84rTcWBA-nWUiWmg"
    static var directionsKey = "AIzaSyAavm-0yC6bQUAw_Zt84rTcWBA-nWUiWmg"
    static var locations : [JSON] = []
    static var timeSlots : [JSON] = []
}
var color: UIColor = UIColor.init(red: 107 / 255.0, green: 127 / 255.0, blue: 252 / 255.0, alpha: 1.0)
func validateEmail(enteredEmail:String) -> Bool {
    
    let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
    return emailPredicate.evaluate(with: enteredEmail)
    
}
