//
//  BookingDetailViewController.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 29/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import SwiftyJSON
import Nuke

class BookingDetailViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    var bookingDetails : [String:JSON]!
    
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var bookingStatus: UILabel!
    @IBOutlet weak var serviceStatusTableConstraintConstant: NSLayoutConstraint!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var total: UILabel!
    @IBOutlet weak var tax: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var workingHours: UILabel!
    @IBOutlet weak var bookingAddress: UILabel!
    @IBOutlet weak var bookingTime: UILabel!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var bookingId: UILabel!
    @IBOutlet weak var bookingName: UILabel!
    @IBOutlet weak var subCategoryName: UILabel!

    @IBOutlet weak var serviceStatusTableView: UITableView!
    
    @IBOutlet weak var trackProviderBtn: UIButton!
    
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var taxLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var workingHoursLbl: UILabel!
    @IBOutlet weak var bookingAddressLbl: UILabel!
    @IBOutlet weak var bookingDateLbl: UILabel!
    @IBOutlet weak var bookingIdLbl: UILabel!
    @IBOutlet weak var bookingNameLbl: UILabel!
    
    var statusArray : [String] = []
    var timeStampArray : [String] = []
    var mycolor = UIColor()
    override func viewDidLoad() {
        super.viewDidLoad()

        print(bookingDetails)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
        }

        let lat = self.bookingDetails["boooking_latitude"]!.doubleValue
        let long = self.bookingDetails["booking_longitude"]!.doubleValue
        
        let styleMapUrl: String = "https://maps.googleapis.com/maps/api/staticmap?sensor=false&size=\(2 * Int(self.mapImageView.frame.size.width))x\(2 * Int(self.mapImageView.frame.size.height))&zoom=15&center=\(lat),\(long)&style=feature:administrative%7Celement:geometry%7Ccolor:0x1d1d1d%7Cweight:1&style=feature:administrative%7Celement:labels.text.fill%7Ccolor:0x93a6b5&style=feature:landscape%7Ccolor:0xeff0f5&style=feature:landscape%7Celement:geometry%7Ccolor:0xdde3e3%7Cvisibility:simplified%7Cweight:0.5&style=feature:landscape%7Celement:labels%7Ccolor:0x1d1d1d%7Cvisibility:simplified%7Cweight:0.5&style=feature:landscape.natural.landcover%7Celement:geometry%7Ccolor:0xfceff9&style=feature:poi%7Celement:geometry%7Ccolor:0xeeeeee&style=feature:poi%7Celement:labels%7Cvisibility:off%7Cweight:0.5&style=feature:poi%7Celement:labels.text%7Ccolor:0x505050%7Cvisibility:off&style=feature:poi.attraction%7Celement:labels%7Cvisibility:off&style=feature:poi.attraction%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.business%7Celement:labels%7Cvisibility:off&style=feature:poi.business%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.government%7Celement:labels%7Cvisibility:off&style=feature:poi.government%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:off&style=feature:poi.medical%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.park%7Celement:geometry%7Ccolor:0xa9de82&style=feature:poi.park%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.place_of_worship%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.school%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:poi.sports_complex%7Celement:labels.text%7Ccolor:0xa6a6a6%7Cvisibility:simplified&style=feature:road%7Celement:geometry%7Ccolor:0xffffff&style=feature:road%7Celement:labels.text%7Ccolor:0xc0c0c0%7Cvisibility:simplified%7Cweight:0.5&style=feature:road%7Celement:labels.text.fill%7Ccolor:0x000000&style=feature:road.highway%7Celement:geometry%7Ccolor:0xf4f4f4%7Cvisibility:simplified&style=feature:road.highway%7Celement:labels.text%7Ccolor:0x1d1d1d%7Cvisibility:simplified&style=feature:road.highway.controlled_access%7Celement:geometry%7Ccolor:0xf4f4f4&style=feature:transit%7Celement:geometry%7Ccolor:0xc0c0c0&style=feature:water%7Celement:geometry%7Ccolor:0xa5c9e1"
        
        statusArray.removeAll()
        timeStampArray.removeAll()
        

        switch bookingDetails["status"]!.stringValue {
        case "Completedjob":
            bookingStatus.text = "Payment pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Waitingforpaymentconfirmation":
            bookingStatus.text = "Payment pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Reviewpending":
            bookingStatus.text = "Review pending"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
            
        case "Finished":
            bookingStatus.text = "Job Completed"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            statusArray.append("Provider Completed Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
            timeStampArray.append(bookingDetails["endjob_timestamp"]!.stringValue)
        case "StarttoCustomerPlace":
            bookingStatus.text = "On the way"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            
        case "Startedjob":
            bookingStatus.text = "Work in progress"
            bookingStatus.textColor = UIColor(red:0.89, green:0.31, blue:0.13, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            statusArray.append("Provider Started to Place")
            statusArray.append("Provider Started Job")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            timeStampArray.append(bookingDetails["StarttoCustomerPlace_time"]!.stringValue)
            timeStampArray.append(bookingDetails["startjob_timestamp"]!.stringValue)
        case "Pending":
            bookingStatus.text = "Pending"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            
        case "Accepted":
            bookingStatus.text = "Accepted"
            bookingStatus.textColor = UIColor(red:0.10, green:0.77, blue:0.49, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Accepted")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Accepted_time"]!.stringValue)
            
        case "Rejected":
            bookingStatus.text = "Rejected"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Rejected")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["Rejected_time"]!.stringValue)
            
        case "CancelledbyProvider":
            bookingStatus.text = "Cancelled by Provider"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Cancelled by Provider")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["CancelledbyProvider_time"]!.stringValue)
        case "CancelledbyUser":
            bookingStatus.text = "Cancelled by User"
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            cancelView.isHidden = true
            statusArray.append("Service Requested")
            statusArray.append("Service Cancelled by User")
            
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
            timeStampArray.append(bookingDetails["CancelledbyUser_time"]!.stringValue)
        default:
            bookingStatus.text = "Pending"
            cancelView.isHidden = true
            bookingStatus.textColor = UIColor(red:0.89, green:0.13, blue:0.19, alpha:1.0)
            statusArray.append("Service Requested")
            timeStampArray.append(bookingDetails["Pending_time"]!.stringValue)
        }
        
        print(styleMapUrl)
        if let url = URL(string: styleMapUrl.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        {
            Nuke.loadImage(with: url, into: self.mapImageView)
        }
        
        serviceStatusTableView.delegate = self
        serviceStatusTableView.dataSource = self
        // Do any additional setup after loading the view.
        
        if(bookingDetails["status"]?.stringValue == "Completedjob" || bookingDetails["status"]?.stringValue == "Waitingforpaymentconfirmation" ||
            bookingDetails["status"]?.stringValue == "Reviewpending" ||
            bookingDetails["status"]?.stringValue == "Finished")
        {
            
            self.trackProviderBtn.isHidden = true
            
            
            
            self.total.text = "$\(bookingDetails["total_cost"]!.stringValue)"
            self.tax.text = bookingDetails["gst_cost"]!.stringValue
            self.price.text = "$\(bookingDetails["cost"]!.stringValue)"
            self.taxLbl.text = "\(bookingDetails["tax_name"]!.stringValue) (\(bookingDetails["gst_percent"]!.stringValue)%)"
            
            
            let workhrs = minutesToHoursMinutes(minutes: bookingDetails["worked_mins"]!.intValue)
            if(workhrs.hours > 0){
                self.workingHours.text = "\(workhrs.hours) Hr & \(workhrs.leftMinutes) mins"
            }
            else{
                self.workingHours.text = "\(workhrs.leftMinutes) mins"
            }
            
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["username"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
            
        }
        else if(bookingDetails["status"]?.stringValue == "StarttoCustomerPlace" || bookingDetails["status"]?.stringValue == "Startedjob"){
            
            self.trackProviderBtn.isHidden = false
            priceViewHeightConstraint.constant = 0
            priceView.layoutIfNeeded()
            scrollView.layoutIfNeeded()
            
            
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["username"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
        }
        else{
            self.trackProviderBtn.isHidden = true
            priceViewHeightConstraint.constant = 0
            priceView.layoutIfNeeded()
            scrollView.layoutIfNeeded()
            self.workingHours.text = bookingDetails["worked_mins"]!.stringValue
            self.bookingAddress.text = bookingDetails["address_line_1"]!.stringValue
            self.bookingTime.text = bookingDetails["timing"]!.stringValue
            self.bookingDate.text = bookingDetails["booking_date"]!.stringValue
            self.bookingId.text = bookingDetails["booking_order_id"]!.stringValue
            self.bookingName.text = bookingDetails["username"]!.stringValue
            self.subCategoryName.text = bookingDetails["sub_category_name"]!.stringValue
        }
        
        let serviceStatusTableViewHeight = 75 * statusArray.count
        serviceStatusTableView.rowHeight = UITableViewAutomaticDimension
        serviceStatusTableView.estimatedRowHeight = 75
        
        
        serviceStatusTableView.frame = CGRect.init(x: serviceStatusTableView.frame.origin.x, y: serviceStatusTableView.frame.origin.y, width: serviceStatusTableView.frame.size.width, height: CGFloat(serviceStatusTableViewHeight))
        serviceStatusTableConstraintConstant.constant = CGFloat(serviceStatusTableViewHeight)
        serviceStatusTableView.reloadData()
        serviceStatusTableView.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ServiceStatusTableViewCell", for: indexPath) as! ServiceStatusTableViewCell
        cell.statusIdentifier.text = statusArray[indexPath.row]
        cell.statusTime.text = getReadableDate(inputDate: timeStampArray[indexPath.row])
        if(indexPath.row == 0)
        {
            cell.statusIdentifier.textColor = UIColor.lightGray
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor.white
            cell.topLine.isHidden = true
            cell.bottomLine.isHidden = false
            if(indexPath.row == statusArray.count - 1){
                cell.bottomLine.isHidden = true
            }
        }
        else if(indexPath.row == statusArray.count-1)
        {
            cell.statusIdentifier.textColor = UIColor(red:0.20, green:0.21, blue:0.28, alpha:1.0)
            
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0)
            cell.topLine.isHidden = false
            cell.bottomLine.isHidden = true
        }
        else{
            cell.statusIdentifier.textColor = UIColor.lightGray
            cell.centerCircle.layer.borderWidth = 1
            cell.centerCircle.layer.borderColor = UIColor(red:0.42, green:0.50, blue:0.99, alpha:1.0).cgColor
            cell.centerCircle.backgroundColor = UIColor.white
            cell.topLine.isHidden = false
            cell.bottomLine.isHidden = false
        }
        return cell
    }
    
    func getReadableDate(inputDate : String) -> String
    {
        print(inputDate)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" //Your date format
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+05:30") //Current time zone
        if let date = dateFormatter.date(from: inputDate) //according to date format your date string
        {
            print(date) //Convert String to Date
        
            dateFormatter.dateFormat = "d MMM, yyyy HH:mm a" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date) //pass Date here
            
                print(newDate) //New formatted Date string
                return newDate
        }
        else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statusArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    

    func minutesToHoursMinutes (minutes : Int) -> (hours : Int , leftMinutes : Int) {
        return (minutes / 60, (minutes % 60))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancelBooking(_ sender: Any) {
        print("cancel")
    }
    @IBAction func trackProvider(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TrackingViewController") as! TrackingViewController
        vc.bookingDetails = self.bookingDetails
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

}
