//
//  SubCategoriesViewController.swift
//  UberdooX
//
//  Created by Karthik Sakthivel on 15/10/17.
//  Copyright © 2017 Uberdoo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SwiftSpinner
import Nuke

class SubCategoriesViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {

    var subCategoryId : String!
    var subcategories : [JSON] = []
    var selectedId : String!
    var mycolor = UIColor()
    @IBOutlet weak var subCategoryCollectionView: UICollectionView!
     var needReload = true
    override func viewDidLoad() {
        super.viewDidLoad()
        
        subCategoryCollectionView.delegate = self
        subCategoryCollectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        if UserDefaults.standard.object(forKey: "myColor") != nil
        {
            //            mycolor = UserDefaults.standard.object(forKey: "mycolor")as! UIColor
            let colorData = UserDefaults.standard.object(forKey: "myColor") as! Data
            //            var color: UIColor? = nil
            mycolor = NSKeyedUnarchiver.unarchiveObject(with: colorData) as! UIColor
            
        }
        
    }
    override func viewDidAppear(_ animated: Bool) {
        if(needReload){
            needReload = false
            getSubCategories()
        }
    }
    func showAlert(title: String,msg : String)
    {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    func getSubCategories(){
        var headers : HTTPHeaders!
        

        
        if let accesstoken = UserDefaults.standard.string(forKey: "access_token") as String?
        {
            headers = [
                "Authorization": accesstoken,
                "Accept": "application/json"
            ]
        }
        else
        {
            headers = [
                "Authorization": "",
                "Accept": "application/json"
            ]
        }
        
        let params: Parameters = [
            "id": self.subCategoryId
        ]
        
        
        
        print("accesstoken = ",UserDefaults.standard.string(forKey: "access_token") as String?);
        
        
        print("Parameters = ",params);
        
        
        SwiftSpinner.show("Fetching Services...")
        let url = "\(Constants.baseURL)/api/list_subcategory"
        Alamofire.request(url,method: .post, parameters:params, headers:headers).responseJSON { response in
            
            if(response.result.isSuccess)
            {
                SwiftSpinner.hide()
                if let json = response.result.value {
                    print("SUB CATEGORY JSON: \(json)") // serialized json response
                    let jsonResponse = JSON(json)
                    if(jsonResponse["error"].stringValue == "true" )
                    {
                        self.showAlert(title: "Oops", msg: jsonResponse["error_message"].stringValue)
                    }
                    else if(jsonResponse["error"].stringValue == "Unauthenticated")
                    {
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
                        self.present(vc, animated: true, completion: nil)
                    }
                    else{
                        self.subcategories = jsonResponse["list_subcategory"].arrayValue
                        self.subCategoryCollectionView.reloadData()
                    }
                }
            }
            else{
                SwiftSpinner.hide()
                print(response.error.debugDescription)
                self.showAlert(title: "Oops", msg: response.error!.localizedDescription)
                
            }
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubCategoryCollectionViewCell", for: indexPath) as! SubCategoryCollectionViewCell
        
        cell.subCategoryName.text = self.subcategories[indexPath.row]["sub_category_name"].stringValue
        let imageURL = self.subcategories[indexPath.row]["icon"].stringValue
        
        if imageURL == " "
        {
            
        }
        else
        {
            Nuke.loadImage(with:URL(string:imageURL)!, into: cell.subCategoryImage)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc  = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimeAndAddressViewController") as! SelectTimeAndAddressViewController        
        vc.selectedSubCategoryId = self.subcategories[indexPath.row]["id"].stringValue
        vc.selectedSubCategoryName = self.subcategories[indexPath.row]["sub_category_name"].stringValue
        self.present(vc, animated: true, completion: nil)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.subcategories.count
    }
    
    
}
